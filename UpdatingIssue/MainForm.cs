﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UpdatingIssue
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            var addNewElementsTask = new Task(() => AddNewElements());
            addNewElementsTask.Start();
        }

        private string GetRandomString(int charactersCount)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            var res = new string(
                Enumerable.Repeat(chars, charactersCount)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return res;
        }

        private void AddNewElements()
        {
            while (true)
            {
                someDictionary.Add(GetRandomString(8), GetRandomString(8));

                Invoke((MethodInvoker)delegate
                {
                    listBox.DataSource = new BindingSource(someDictionary, null);
                    listBox.DisplayMember = "Value";
                    listBox.ValueMember = "Key";
                });

                Thread.Sleep(TimeSpan.FromMilliseconds(500));
            }
        }

        private Dictionary<string, string> someDictionary = new Dictionary<string, string>();
    }
}
